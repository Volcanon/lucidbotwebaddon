var options=require("sdk/simple-prefs");
var req=require("sdk/request").Request;
var s = require("sdk/self");
var notifications=require("sdk/notifications");
var tabs=require("sdk/tabs");
var timers=require("sdk/timers");
var pageMod=require("sdk/page-mod");
var utourl="http://utopia-game.com/wol/";
var debug = options.prefs.debug;
var page = '';

var uto_include = /.*utopia-game\.com.+(throne|sorcery|thievery|enchantment|science|send_armies|kingdom_details|kingdom_news|council_military|council_internal).*/;

var lastSOMTime = 0;
var lastSOTTime = 0;
var lastSurveyTime = 0;
var lastSOSTime = 0;
var lastNewsTime = 0;

pageMod.PageMod({
    include: uto_include,
    contentScriptFile: s.data.url("theonescript.js"),
    attachTo: 'top',
    onAttach: function(worker) {
        worker.port.emit("retrieve", tabs.activeTab.url);

    worker.port.on('data', function(postData) {
    var debug = options.prefs.debug;
    var validURL = checkURL(tabs.activeTab.url);
    if (debug) {
        console.error("getPage is getting " + tabs.activeTab.url);
    }
    if (validURL) {
        var pageScript = "general.js";
        var location = "intel";
        var utools = options.prefs.utools;
        if (page == "sorcery") {
            pageScript = "magicops.js";
            location = "spells_ops"
        }
        if (page == "enchantment") {
            pageScript = "selfspells.js";
            location = "spells_ops"
        }
        if (page == "thievery") {
            pageScript = "thiefops.js";
            location = "spells_ops"
        }
        if (page == 'kdscreen') {
            pageScript = "general.js"
        }
        if (page == 'kdnews') {
            pageScript = "general.js";
            location = 'news';
        }
        if (page == 'science') {
            pageScript = "general.js";
            location = "intel";
        }
        if (page == 'survey') {
            pageScript = "general.js";
            location = "intel";
        }
        if (page == 'som') {
            pageScript = "general.js";
            location = "intel";
        }
        if (page == 'kdpage') {
            pageScript = "general.js";
            location = "intel";
        }
        if (page == 'attack') {
            pageScript = "general.js";
            location = "attacking";
        }
        if (page == 'throne') {
            if (utools) {
                pageScript = "throne.js";
            }
            else {
                pageScript = "general.js";
            }
            location = "intel";
        }
        if (page == 'dragon') {
            pageScript = "general.js";
            location = "dragon";
        }
        if (page == 'aid') {
            pageScript = "general.js";
            location = "aiding";
        }
        console.error("Found something useful - using theonescript.js and sending to " + location);
        postToLucidBot(location, postData);
    }
    })}});

function checkURL(url){
    var validURL = false;
    try {
        var now = new Date();
        var nowmillis = now.getTime();
        var interval = options.prefs.interval;
        var utools = options.prefs.utools;
        var selfintel = options.prefs.selfintel;
        var debug = options.prefs.debug;
        var attacks = options.prefs.attacks;
        var dragon = options.prefs.dragon;
        var aid = options.prefs.aid;

        if (debug) {
            console.error("checkURL is checking: " + url)
        }
        if (url.indexOf(utourl) == -1) {
            console.error("Not a Utopia Game URL - skipping");
            return validURL
        }
        else {
            if (url.indexOf("/sorcery") != -1) {
                page = "sorcery";
                validURL = true
            }
            if (url.indexOf("/enchantment") != -1) {
                page = "enchantment";
                if (selfspells) {
                    validURL = true
                }
            }
            if (url.indexOf("/thievery") != -1) {
                page = "thievery";
                validURL = true
            }
            if (url.indexOf("/kingdom_news") != -1) {
                page = "kdnews";
                if (nowmillis > (lastNewsTime + (interval * 60 * 60 * 1000))){
                    lastNewsTime = nowmillis;
                    validURL = true
                }
            }
            if (url.indexOf("/science") != -1) {
                page = "science";
                if (nowmillis > (lastSOSTime + (interval * 60 * 60 * 1000)) && selfintel){
                    lastSOSTime = nowmillis;
                    validURL = true
                }
            }
            if (url.indexOf("/council_internal") != -1) {
                page = "survey";
                if (nowmillis > (lastSurveyTime + (interval * 60 * 60 * 1000)) && selfintel){
                    lastSurveyTime = nowmillis;
                    validURL = true
                }
            }
            if (url.indexOf("/council_military") != -1) {
                page = "som";
                if (nowmillis > (lastSOMTime + (interval * 60 * 60 * 1000)) && selfintel){
                    lastSOMTime = nowmillis;
                    validURL = true
                }
            }
            if (url.indexOf("/kingdom_details") != -1) {
                page = "kdpage";
                //if (utools) {
                //    return false;
                //}
                validURL = false
            }
            if (url.indexOf("/send_armies") != -1) {
                page = "attack";
                if (attacks == true) {
                    validURL = true
                }
            }
            if (url.indexOf("/throne") != -1) {
                page = "throne";
                if (nowmillis > (lastSOTTime + (interval * 60 * 60 * 1000)) && selfintel){
                    lastSOTTime = nowmillis;
                    validURL = true
                }
            }
            if (url.indexOf("/attack_dragon") != -1 || url.indexOf("/fund_dragon") != -1) {
                page = "dragon";
                if (dragon == true) {
                    validURL = true
                }
            }
            if (url.indexOf("/aid") != -1) {
                page = "aid";
                if (aid == true) {
                    validURL = true
                }
            }
        }
    }
    catch(e){
        if (debug){
            console.error("An error occurred while checking page data. Error - "+e)}
    }

    if(debug){
        console.error("validURL = " + validURL)
    }
    return validURL
}

function postToLucidBot(location, postData) {
    try {

        var lucidbot = options.prefs.lucidbot_url.trim().replace('http://', '').replace('/ForumAgent/', '/api/');
        if (!lucidbot.endsWith("/")) { lucidbot = lucidbot + "/"; }
        var username = options.prefs.username;
        var password = options.prefs.password;
        var debug = options.prefs.debug;
        var rawText = postData.split("||^||");
        var postResult = 0;

        if (debug) {
            console.error("Posting to LucidBot")
        }
        if (page == "thievery") {
            if (rawText[0].indexOf(" confidence in the information retrieved.")!=-1) {
                location = "intel";
                rawText[1] = '';
                rawText[2] = 'Null';
                console.error("Thievery op is actually intel, moving it to the appropriate place");
            }
            if (rawText[0].indexOf("Early indications show that our operation was a success")==-1){
                console.error("No valid data to send on page " + page + " Aborting");
                return
            }
        }

        if (page == "sorcery" || page == "enchantment") {
            if (rawText[0].indexOf("and the spell succeeds")==-1) {
                console.error("No valid data to send on page " + page + " Aborting");
                return
            }
        }

        if (page == "attack") {
            if (rawText[0].indexOf("Your forces arrive at") == -1 ||
                rawText[0].indexOf("it appears our army was much too weak to break their defenses") ||
                rawText[0].indexOf("Our army appears to have failed") ||
                rawText[0].indexOf("Your troops march onto the battlefield and are quickly driven back, unable to break through")) {
                console.error("No valid attack on page " + page + " Aborting");
                return
            }
        }

        var url = "http://" + username + ":" + password + "@" + lucidbot + location;

        try {
            if (rawText[1].length > 0) {
                url = url + "?targetName=" + rawText[1];
            }
            if (rawText[2] != "Null") {
                url = url + "&targetKingdom=" + rawText[2].replace(':', '%3A');
            }
        }
        catch (e) {
            console.error('Error appending to URL - so using it as it is.  Error code: ' + e);
        }

        if (debug) {
            console.error(postData);
            console.error("URL is " + url);
        }

        var request = req({
            headers: {'content-type': 'text/plain'},
            url: url,
            content: rawText[0],
            onComplete: function(response) {
                postResult = response.status;
                console.error("Response: " + response.status);
                console.error("Sending to url " + url);
                if (postResult == 200 || postResult == 204) {
                    sendNotice("Successfully posted " + page + " data of type " + location)
                }
                else {
                    sendNotice("Did NOT successfully post " + page + " of type " + location);
                }

            }
        });

        request.post();

    }
    catch (error) {
        console.error("Error Sending Data. Error - "+ error + lucidbot + username + password);
        sendNotice("ERROR: Did NOT successfully post " + page + " of type " + location);
    }
}


function sendNotice(text){
    var noticeBool = options.prefs.notice;
    if(noticeBool){
        notifications.notify({title:"LucidBot Web Addon",text:text, data:""})
    }
}


