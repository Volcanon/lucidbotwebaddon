/**
 * Created by Kyle on 10/18/2014.
 */


var content = document.body.textContent;
var tprov = getSpellTarget(false);
var lastprov = sessionStorage["spell_lastprov"];

if ( tprov.length == 0 && typeof(lastprov) != "undefined" ) {
    tprov = lastprov;
}

if (content.indexOf('http://utopia-game.com/wol/game/kingdom_details/' != -1)) {
    var kdloc = /\d+:\d+/.exec(content);
    content = content.replace('(<a href="http://utopia-game.com/wol/game/kingdom_details/\d+/\d+">\d+:\d+</a>)', kdloc);
    content = content.replace(/\s+/g, ' ');

    var postData = content + "||^||" + tprov;
}
else {
    var postData = content + "||^||" + tprov;
}

try
{
    var inputList = document.getElementsByTagName("input");
    for ( i = 0; i < inputList.length; i++ ) {
        var item = inputList[i];

        if ( item.value == "Cast Spell" ) {
            item.onclick = function () {
                getSpellTarget(true);
            };
        }
    }
}
catch ( e ) {
    console.error(e)
}

self.port.emit('alert', postData);

function getSpellTarget(click)
{
    var tprov = "";

    try {
        prov = document.getElementById("id_target_province");
        index = prov.selectedIndex;
        tprov = prov[index].text;

        if (click) {
            sessionStorage["spell_lastprov"] = tprov;
        }
    }
    catch ( e ) {
        console.error(e)
    }

    return tprov;
}