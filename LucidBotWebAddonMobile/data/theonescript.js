/**
 * Created by Kyle on 3/15/2015.
 */

self.port.on('retrieve', function (url) {
    var postData = document.body.textContent;
    var kdloc = "Null";

    if (url.indexOf("/sorcery") != -1) {
        var tprov = getSpellTarget(false);
        var lastprov = sessionStorage["spell_lastprov"];

        if ( tprov.length == 0 && typeof(lastprov) != "undefined" ) {
            tprov = lastprov;
        }

        if (postData.indexOf('http://utopia-game.com/wol/game/kingdom_details/' != -1)) {
            kdloc = /\d+:\d+/.exec(postData);
            postData = postData.replace('(<a href="http://utopia-game.com/wol/game/kingdom_details/\d+/\d+">\d+:\d+</a>)', kdloc);
            postData = postData.replace(/\s+/g, ' ');
        }

        postData = postData + "||^||" + tprov + "||^||" + kdloc;

        try
        {
            var inputList = document.getElementsByTagName("input");
            for ( i = 0; i < inputList.length; i++ ) {
                var item = inputList[i];

                if ( item.value == "Cast Spell" ) {
                    item.onclick = function () {
                        getSpellTarget(true);
                    };
                }
            }
        }
        catch ( e ) {
            console.error(e)
        }
    }

    else if (url.indexOf("/enchantment") != -1) {
        postData = postData.replace(/\s+/g, ' ');
    }

    else if (url.indexOf("/thievery") != -1) {

        var tprov = getThiefTarget(false);
        var lastprov = sessionStorage["thief_lastprov"];

        if (tprov.length == 0 && typeof(lastprov) != "undefined") {
            tprov = lastprov;
        }

        kdloc = /\d+:\d+/.exec(postData);
        postData = postData.replace(/\s+/g, ' ');

        postData = postData + "||^||" + tprov + "||^||" + kdloc;

        try {
            var inputList = document.getElementsByTagName("input");
            for (i = 0; i < inputList.length; i++) {
                var item = inputList[i];

                if (item.value == "Run operation") {
                    item.onclick = function () {
                        getThiefTarget(true);
                    };
                }
            }
        }
        catch (e) {
            console.error(e)
        }
    }

    self.port.emit('data', postData);
});




function getSpellTarget(click)
{
    var tprov = "";

    try {
        prov = document.getElementById("id_target_province");
        index = prov.selectedIndex;
        tprov = prov[index].text;

        if (click) {
            sessionStorage["spell_lastprov"] = tprov;
        }
    }
    catch ( e ) {
        console.error(e)
    }

    return tprov;
}


function getThiefTarget(click)
{
    var tprov = "";

    try {
        prov = document.getElementById("id_target_province");
        index = prov.selectedIndex;
        tprov = prov[index].text;

        if (click) {
            sessionStorage["thief_lastprov"] = tprov;
        }
    }
    catch ( e ) {
        console.error(e)
    }

    return tprov;
}





