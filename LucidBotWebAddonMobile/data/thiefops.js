/**
 * Created by Kyle on 10/18/2014.
 */


var content = document.body.textContent;
var tprov = getThiefTarget(false);
var lastprov = sessionStorage["thief_lastprov"];

if ( tprov.length == 0 && typeof(lastprov) != "undefined" ) {
    tprov = lastprov;
}

content = content.replace(/\s+/g, ' ');

var postData = content + "||^||" + tprov;

try
{
    var inputList = document.getElementsByTagName("input");
    for ( i = 0; i < inputList.length; i++ ) {
        var item = inputList[i];

        if ( item.value == "Run operation" ) {
            item.onclick = function () {
                getThiefTarget(true);
            };
        }
    }
}
catch ( e ) {
    console.error(e)
}

self.port.emit('alert', postData);

function getThiefTarget(click)
{
    var tprov = "";

    try {
        prov = document.getElementById("id_target_province");
        index = prov.selectedIndex;
        tprov = prov[index].text;

        if (click) {
            sessionStorage["thief_lastprov"] = tprov;
        }
    }
    catch ( e ) {
        console.error(e)
    }

    return tprov;
}