Hey!

This is a browser addon for the Utopia IRC Bot [LucidBot](https://bitbucket.org/fredrik_yttergren/lucidbot/wiki/Home)

Currently supports Firefox Mobile/PC.

You can find me logged onto irc.utonet.org as Frylock

Feel free to log any bugs/feature requests over in the Issues area on the left hand side