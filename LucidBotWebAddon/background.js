/**
 * Created by Kyle on 1/28/2017.
 */


browser.runtime.onMessage.addListener(function(request, sender, sendResponse) {

    if (request.method == "getSettings") {
        sendResponse(localStorage);
    }
    else if (request.method == "notice") {
        var opt = {
            type: "basic",
            iconUrl: "favicon.ico",
            title: request.title,
            message: request.msg
        };
        browser.notifications.create("notice", opt, function (id) {
            }
        );
        setTimeout(function(){ browser.notifications.clear("notice", function(){}); }, 3000);
        sendResponse({});
    }
    else if (request.method = "setSetting") {
        localStorage[request.key] = request.data;
    }
    else
        sendResponse({});
});
