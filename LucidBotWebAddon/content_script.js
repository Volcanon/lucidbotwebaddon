/**
 * Created by Kyle on 1/28/2017.
 */

var utourl="https://utopia-game.com/wol/";
var settings;

browser.runtime.sendMessage({method:"getSettings"}, function(response) {
    settings = response;
    main();
});


function sorcery(postData) {
    var tprov = getSpellTarget(false);
    var lastprov = sessionStorage["spell_lastprov"];
    var kdloc = "Null";

    if (tprov.length == 0 && typeof(lastprov) != "undefined") {
        tprov = lastprov;
    }

    if (postData.indexOf('https://utopia-game.com/wol/game/kingdom_details/' != -1)) {
        kdloc = /\d+:\d+/.exec(postData);
        postData = postData.replace('(<a href="https://utopia-game.com/wol/game/kingdom_details/\d+/\d+">\d+:\d+</a>)', kdloc);
        postData = postData.replace(/\s+/g, ' ');
    }

    postData = postData + "||^||" + tprov + "||^||" + kdloc;

    try {
        var inputList = document.getElementsByTagName("input");
        for (i = 0; i < inputList.length; i++) {
            var item = inputList[i];

            if (item.value == "Cast Spell") {
                item.onclick = function () {
                    getSpellTarget(true);
                };
            }
        }
    }
    catch (e) {
        logger(e)
    }

    return postData;
}

function enchantment(postData) {

    postData = postData.replace(/\s+/g, ' ');

    return postData
}

function thievery(postData) {
    var tprov = getThiefTarget(false);
    var lastprov = sessionStorage["thief_lastprov"];
    var kdloc;

    if (tprov.length == 0 && typeof(lastprov) != "undefined") {
        tprov = lastprov;
    }

    kdloc = /\d+:\d+/.exec(postData);
    postData = postData.replace(/\s+/g, ' ');

    postData = postData + "||^||" + tprov + "||^||" + kdloc;

    try {
        var inputList = document.getElementsByTagName("input");
        for (i = 0; i < inputList.length; i++) {
            var item = inputList[i];

            if (item.value == "Run operation") {
                item.onclick = function () {
                    getThiefTarget(true);
                };
            }
        }
    }
    catch (e) {
        logger(e)
    }

    return postData;
}


function getSpellTarget(click)
{
    var tprov = "";

    try {
        prov = document.getElementById("id_target_province");
        index = prov.selectedIndex;
        tprov = prov[index].text;

        if (click) {
            sessionStorage["spell_lastprov"] = tprov;
        }
    }
    catch ( e ) {
        logger(e)
    }

    return tprov;
}


function getThiefTarget(click)
{
    var tprov = "";

    try {
        prov = document.getElementById("id_target_province");
        index = prov.selectedIndex;
        tprov = prov[index].text;

        if (click) {
            sessionStorage["thief_lastprov"] = tprov;
        }
    }
    catch ( e ) {
        logger(e);
    }

    return tprov;
}

function toBool(value) {
    if (value === "true") {
        return true
    }
    if (value === "false") {
        return false
    }
    else {
        return value
    }
}

function checkURL(url){
    var validURL = false;
    var page = false;

    try {
        var utools = settings['preferences.utools'];
        var attacks = settings['preferences.attacks'];
        var dragon = settings['preferences.dragon'];
        var aid = settings['preferences.aid'];
        var state_council = settings['preferences.state_council'];
        var self_spells = settings['preferences.selfspells'];
        var kingdom_ritual = settings['preferences.kingdom_ritual'];

        logger("checkURL is checking: " + url);
        logger();

        if (url.indexOf(utourl) == -1) {
            logger("Not a Utopia Game URL - skipping");
            return validURL
        }
        else {
            if (url.indexOf("/sorcery") != -1 || url.indexOf("/charms") != -1) {
                page = "sorcery";
                validURL = true
            }
            if (url.indexOf("/enchantment") != -1) {
                page = "enchantment";
                if (self_spells == "true") {
                    validURL = true
                }
            }
            if (url.indexOf("/thievery") != -1) {
                page = "thievery";
                validURL = true
            }
            if (url.indexOf("/kingdom_news") != -1) {
                page = "kdnews";
                validURL = timerCheck('lastNewsTime');
            }
            if (url.indexOf("/science") != -1) {
                page = "science";
                validURL = timerCheck('lastSOSTime');
            }
            if (url.indexOf("/council_internal") != -1) {
                page = "survey";
                validURL = timerCheck('lastSurveyTime');
            }
            if (url.indexOf("/council_military") != -1) {
                page = "som";
                validURL = timerCheck('lastSOMTime');
            }
            if (url.indexOf("/kingdom_details") != -1) {
                page = "kdpage";
                if (settings['preferences.kingdoms'] == "true" && settings['preferences.utools'] == "false") {
                    validURL = true
                }
            }
            if (url.indexOf("/send_armies") != -1) {
                page = "attack";
                if (attacks == "true") {
                    validURL = true
                }
            }
            if (url.indexOf("/throne") != -1) {
                page = "throne";
                validURL = timerCheck('lastSOTTime');
            }
            if (url.indexOf("/attack_dragon") != -1 || url.indexOf("/fund_dragon") != -1) {
                page = "dragon";
                if (dragon == "true") {
                    validURL = true
                }
            }
            if (url.indexOf("/aid") != -1) {
                page = "aid";
                if (aid == "true") {
                    validURL = true
                }
            }
            if (url.indexOf("/council_state") != -1) {
                page = "state_council";
                if (state_council == "true") {
                    validURL = timerCheck('lastStateCouncilTime');
                }
            }
            if (url.indexOf("/cast_ritual") != -1 || url.indexOf("/status_ritual") != -1) {
                page = "kingdom_ritual";
                if (kingdom_ritual == "true") {
                    validURL = true;
                }
            }
        }
    }
    catch(e){
        console.error("An error occurred while checking page data. Error - "+e)
    }

    logger("validURL = " + validURL);

    return [validURL, page];
}

function getWebPage(pageType) {
    var parser = new DOMParser();
    var url = utourl + "game" + pageType;
    var location = "intel";

    if (pageType == "/kingdom_news") { location = "news"}
    
    logger("Getting Web page: " + url);

    jQuery.ajax({
        type: "GET",
        url: url,
        dataType: "html",
        success: function(data) {
            var doc = parser.parseFromString(data, "text/html");
            postToLucidBot(location, pageType, doc.body.textContent, false);
        },
        error: function(data) {
            logger("Response: " + data.text);
        }
    });
}

function timerCheck(name) {
    var now = new Date();
    var nowmillis = now.getTime();
    var interval = parseInt(settings['preferences.interval']);
    var selfintel = settings['preferences.selfintel'];

    if (nowmillis > (parseInt(settings[name]) + (interval * 60 * 60 * 1000)) && selfintel){
        updateSetting(name, nowmillis);
        return true
    }
    return false
}

function logger(msg) {
    if (settings['preferences.debug'] == "true") {
        console.log(msg)
    }
}

function postToLucidBot(location, page, postData, notice) {
    try {
        var httpRegex = new RegExp('https://', 'gi');
        var faRegex = new RegExp('/ForumAgent/', 'gi');
        var lucidbot = settings['preferences.lucidbot_url'];
        lucidbot = trim(lucidbot);
        lucidbot = lucidbot.replace(httpRegex, '');
        lucidbot = lucidbot.replace(faRegex, '/api/');
        if (!lucidbot.endsWith("/")) {
            lucidbot = lucidbot + "/";
        }
        var username = settings['preferences.username'];
        var password = settings['preferences.password'];
        var rawText = postData.split("||^||");

        if (page == "thievery") {
            if (rawText[0].indexOf(" confidence in the information retrieved.") != -1) {
                location = "intel";
                rawText[1] = '';
                rawText[2] = 'Null';
                logger("Thievery op is actually intel, moving it to the appropriate place");
            }
            if (rawText[0].indexOf(" Our thieves have stolen the last 2 month's of kingdom news") != -1) {
                location = "news";
                rawText[1] = '';
                rawText[2] = 'Null';
                logger("Thievery op is actually news, moving it to the appropriate place");
            }
            if (rawText[0].indexOf("Early indications show that our operation was a success") == -1) {
                logger("No valid data to send on page " + page + " Aborting");
                return
            }
        }
        if (page == "sorcery" || page == "enchantment") {
            if (rawText[0].indexOf("and the spell succeeds") == -1) {
                logger("No valid data to send on page " + page + " Aborting");
                return
            }
        }

        if (page == "attack") {
            if (rawText[0].indexOf("Your forces arrive at") == -1 &&
                rawText[0].indexOf("it appears our army was much too weak to break their defenses") == -1 &&
                rawText[0].indexOf("Our army appears to have failed") == -1 &&
                rawText[0].indexOf("Your troops march onto the battlefield and are quickly driven back, unable to break through") == -1 &&
                rawText[0].indexOf("Your army was no match for the defenses of") == -1) {
                logger("No valid attack on page " + page + " Aborting");
                return
            }
        }

        var url = "https://" + lucidbot + location;

        try {
            if (typeof rawText[1] != "undefined") {
                url += "?targetName=" + rawText[1];
                if (rawText[2] != "Null") {
                    url += "&targetKingdom=" + rawText[2].replace(':', '%3A');
                }
            }
            if (location == "spells_ops") {
                if (url.indexOf("&") == -1) {
                    url += "?single=true"
                } else {
                    url += "&single=true";
                }
            }
        }
        catch (e) {
            logger(e);
        }
        
        logger(postData);
        logger(url);

        jQuery.ajax({
            type: "POST",
            url: url,
            contentType: 'text/plain',
            data: postData,
            headers: {
                "Authorization": "Basic " + btoa(username + ":" + password)
            },
            success: function(response) {
                logger("Response: " + response.text + " " + response.status);
                logger("Sending to url " + url);
                if (notice == "true") {
                    sendNotice("Successfully posted " + page + " data of type " + location)
                }
            },
            error: function(response) {
                logger("Response: " + response.text + " " + response.status);
                if (notice == "true") {
                    sendNotice("Did NOT successfully post " + page + " of type " + location);
                }
            }
        });
    }
    catch (error) {
        logger("Error Sending Data. Error - "+ error);
        sendNotice("ERROR: Did NOT successfully post " + page + " of type " + location);
    }
}


function sendNotice(msg) {
    var noticeBool = settings['preferences.notice'];
    if (noticeBool == "true")	{
        logger("Sending notice with message: " + msg);
        browser.runtime.sendMessage({
            method: "notice", title: "LucidBot", msg: msg
        })
    }
}

function updateSetting(key, data) {
    logger("Updating Setting " + key + "With value " + data);
    browser.runtime.sendMessage({
        method: "setSetting", key: key, data: data
    })
}


function trim(str) {
    var space_strip = new RegExp('^\s+|\s+$', 'gm');
    return str.replace(space_strip,'');
}

//function auto_fill(page_data) {
//    var kd_regex = new RegExp(/\/wol\/game\/kingdom_details\/(\d+)\/(\d+)/);
//    var matcher = kd_regex.exec(page_data);
//    var kingdom = matcher[0] + "%3A" + matcher[1];
//
//    var prov = document.getElementById("id_target_province");
//    var index = prov.selectedIndex;
//    var tprov = prov[index].text;
//
//    var attack = document.getElementById("id_attack_type");
//    var attack_index = attack.selectedIndex;
//    var attack_type = attack[attack_index].text;
//
//    var lucidbot = settings['preferences.lucidbot_url'];
//
//    jQuery.ajax({
//        type: "GET",
//        url: lucidbot + "/kingdoms?location=" + kingdom + "&includeIntel=true",
//        dataType: "json",
//        success: function(data) {
//            for (var province in data['kingdoms']['kingdom']['provinces']) {
//                if (data['kingdoms']['kingdom']['provinces'].hasOwnProperty(province)) {
//                    if (province['name'] == tprov) {
//                        var defense = parseInt(province['EstimatedCurrentDefense']);
//
//                        //calculate and fill here
//                        break;
//                    }
//                }
//            }
//
//
//        },
//        error: function(data) {
//            logger("Response: " + data.text);
//        }
//    });
//
//
//}

function main() {
    var now = new Date();
    var nowmillis = now.getTime();
    var utools = settings['preferences.utools'];
    var postData = '';
    var notice = settings['preferences.notice'];
    var pageInfo = checkURL(document.URL.toString());
    var validURL = pageInfo[0];
    var page = pageInfo[1];

    if (validURL) {
        var location = "intel";
        postData = document.body.textContent;

        if (page == 'throne') {
            location = "intel";
            if (settings['preferences.bulk'] == 'true'){
                postToLucidBot(location, page, postData, notice);
                getWebPage("/kingdom_news");
                updateSetting('lastNewsTime', nowmillis);
                setTimeout(getWebPage("/council_military"), 3000);
                updateSetting('lastSOMTime', nowmillis);
                setTimeout(getWebPage("/council_internal"), 3000);
                updateSetting('lastSurveyTime', nowmillis);
                setTimeout(getWebPage("/science"), 3000);
                updateSetting('lastSOSTime', nowmillis);
                setTimeout(getWebPage("/council_state"), 3000);
                updateSetting('lastStateCouncilTime', nowmillis);
            }
        }
        if (page == "sorcery") {
            postData = sorcery(postData);
            location = "spells_ops"
        }
        if (page == "enchantment") {
            postData = enchantment(postData);
            location = "spells_ops"
        }
        if (page == "charms") {
            location = "spells_ops"
        }
        if (page == "thievery") {
            postData = thievery(postData);
            location = "spells_ops"
        }
        if (page == 'kdnews') {
            location = 'news';
        }
        if (page == 'science') {
            location = "intel";
        }
        if (page == 'survey') {
            location = "intel";
        }
        if (page == 'som') {
            location = "intel";
        }
        if (page == 'kdpage') {
            postData = document.body.innerHTML;
            postData = postData.replace(/<\/th>/g, '').replace(/<\/td>/g, '').replace(/<td>/g, '').replace(/<th>/g, '')
                .replace(/<a href="\/wol\/game\/province_profile\/\d+\/\d+\/\d+">/g, '').replace(/<\/a>/g, '').replace(/<div style="float:right;">\d+\.\d+ \w+<\/div>/g, '')
                .replace(/<td class="slot-number">/g, '').replace(/<td class="province-name">/g, '').replace(/<a href="\/wol\/game\/kingdom_details\/\d+\/\d+">/g, '')
                .replace(/<a href="\/wol\/sit\/game\/province_profile\/\d+\/\d+\/\d+">/g, '').replace(/<th class="header">/g, '').replace(/<div class="order-icon"><\/div>/g, '');
            location = "kingdoms";
        }
        if (page == 'attack') {
            //var fblike = doc.getElementsByTagName("tbody");
            //var button = doc.createElement("auto-fill");
            //button.setAttribute("type", "button");
            //button.setAttribute("value", "value");
            //button.setAttribute("name", "auto-fill");
            //var parentDiv = fblike.parentNode;
            //parentDiv.insertBefore(button, fblike);
            //document.getElementById('auto-fill').addEventListener('click', auto_fill);
            location = "attacking";
        }
        if (page == 'dragon') {
            location = "dragon";
        }
        if (page == 'aid') {
            location = "aiding";
        }
        if (page == 'state_council') {
            location = "intel";
        }
        if (page == 'kingdom_ritual') {
            location = "kingdom_ritual";
        }
        if (page == 'throne' && settings['preferences.bulk'] == 'true') {
        } else {
            postToLucidBot(location, page, postData, notice);
        }
    }
}