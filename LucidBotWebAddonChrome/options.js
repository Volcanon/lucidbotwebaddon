/**
 * Created by Kyle on 11/30/2014.
 */

function saveOptions(){
    localStorage['lastSOMTime'] = 0;
    localStorage['lastSOTTime'] = 0;
    localStorage['lastSurveyTime'] = 0;
    localStorage['lastSOSTime'] = 0;
    localStorage['lastNewsTime'] = 0;
    localStorage['lastStateCouncilTime'] = 0;
    localStorage["preferences.lucidbot_url"] = document.getElementById('lucidbot_url').value;
    localStorage["preferences.username"] = document.getElementById('username').value;
    localStorage["preferences.password"] = document.getElementById('password').value;
    localStorage["preferences.interval"] = document.getElementById('interval').value;
    localStorage["preferences.selfintel"] = document.getElementById('selfintel').checked;
    localStorage["preferences.selfspells"] = document.getElementById('selfspells').checked;
    localStorage["preferences.attacks"] = document.getElementById('attacks').checked;
    localStorage["preferences.dragon"] = document.getElementById('dragon').checked;
    localStorage["preferences.aid"] = document.getElementById('aid').checked;
    localStorage["preferences.kingdoms"] = document.getElementById('kingdoms').checked;
    localStorage["preferences.state_council"] = document.getElementById('state_council').checked;
    localStorage["preferences.ritual"] = document.getElementById('ritual').checked;
    localStorage["preferences.notice"] = document.getElementById('notice').checked;
    localStorage["preferences.bulk"] = document.getElementById('bulk').checked;
    localStorage["preferences.debug"] = document.getElementById('debug').checked;

    var status = document.getElementById("status");
    status.innerText = "Options Saved.";
    setTimeout(function() {document.getElementById("status").innerText = "";}, 750);
}

function restoreOptions() {

    document.getElementById('lucidbot_url').value =  localStorage["preferences.lucidbot_url"] || "https://bot.whatever.com:49999/api/";
    document.getElementById('username').value = localStorage["preferences.username"] || "Username";
    document.getElementById('password').value = localStorage["preferences.password"] || "password";
    document.getElementById('interval').value = localStorage["preferences.interval"] || 1;
    document.getElementById('selfintel').checked = checkBool(localStorage["preferences.selfintel"]) || true;
    document.getElementById('selfspells').checked = checkBool(localStorage["preferences.selfspells"]) || true;
    document.getElementById('attacks').checked = checkBool(localStorage["preferences.attacks"]) || true;
    document.getElementById('dragon').checked = checkBool(localStorage["preferences.dragon"]) || true;
    document.getElementById('aid').checked = checkBool(localStorage["preferences.aid"]) || true;
    document.getElementById('kingdoms').checked = checkBool(localStorage["preferences.kingdoms"]) || true;
    document.getElementById('state_council').checked = checkBool(localStorage["preferences.state_council"]) || true;
    document.getElementById('ritual').checked = checkBool(localStorage["preferences.ritual"]) || true;
    document.getElementById('notice').checked = checkBool(localStorage["preferences.notice"]) || true;
    document.getElementById('bulk').checked = checkBool(localStorage["preferences.bulk"]) || true;
    document.getElementById('debug').checked = checkBool(localStorage["preferences.debug"]) || false;

}

function resetTimers() {
    localStorage['lastSOMTime'] = 0;
    localStorage['lastSOTTime'] = 0;
    localStorage['lastSurveyTime'] = 0;
    localStorage['lastSOSTime'] = 0;
    localStorage['lastNewsTime'] = 0;
    localStorage['lastStateCouncilTime'] = 0;

    var timers = document.getElementById("timers");
    timers.innerText = "Timers Reset.";
    setTimeout(function() {document.getElementById("timers").innerText = "";}, 750);
}

function checkBool(value) {
    if (value === "true") {
        return true
    }
    if (value === "false") {
        return false
    }
    else {
        return value
    }
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.getElementById('reset').addEventListener('click', resetTimers);
document.getElementById('save').addEventListener('click', saveOptions);
