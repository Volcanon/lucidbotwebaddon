/**
 * Created by Kyle on 5/15/2015.
 */

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.method == "getSettings") {
        sendResponse(localStorage);
    }
    else if (request.method == "notice") {
        var opt = {
            type: "basic",
            iconUrl: "favicon.ico",
            title: request.title,
            message: request.msg
        };
        chrome.notifications.create("notice", opt, function (id) {
            }
        );
        setTimeout(function(){ chrome.notifications.clear("notice", function(){}); }, 3000);
        sendResponse({});
    }
    else if (request.method = "setSetting") {
        localStorage[request.key] = request.data;
    }
    else
        sendResponse({}); // snub them.
});
